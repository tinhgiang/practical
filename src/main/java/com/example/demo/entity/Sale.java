package com.example.demo.entity;

import javax.persistence.*;
import java.util.Calendar;

@Entity
public class Sale {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int slNo;
    private int salesmanId;
    private String salesmanName;
    private long dos;
    @OneToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "ProID")
    private Product product;

    public Sale() {
        this.dos = Calendar.getInstance().getTimeInMillis();
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getSlNo() {
        return slNo;
    }

    public void setSlNo(int slNo) {
        this.slNo = slNo;
    }

    public int getSalesmanId() {
        return salesmanId;
    }

    public void setSalesmanId(int salesmanId) {
        this.salesmanId = salesmanId;
    }

    public String getSalesmanName() {
        return salesmanName;
    }

    public void setSalesmanName(String salesmanName) {
        this.salesmanName = salesmanName;
    }

    public long getDos() {
        return dos;
    }

    public void setDos(long dos) {
        this.dos = dos;
    }
}
