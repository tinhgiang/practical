package com.example.demo.controller;

import com.example.demo.entity.Product;
import com.example.demo.entity.Sale;
import com.example.demo.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value = "/sales")
public class SaleController {
    @Autowired
    ProductService productService;

    @RequestMapping(method = RequestMethod.GET, value = "/detail")
    public String get(Model model) {
        Sale sale = productService.getSaltById(1);
        model.addAttribute("sale", sale);
        model.addAttribute("product", sale.getProduct());
        return "detail";
    }


    @RequestMapping(method = RequestMethod.GET, value = "/create")
    public String create(Model model) {
        model.addAttribute("product", new Product());
        return "form";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/create")
    public String store(Product product) {
        Sale sale = productService.getSaltById(1);
        product.setSale(sale);
        sale.setProduct(product);
        productService.store(product);
        return "detail";
    }
}
