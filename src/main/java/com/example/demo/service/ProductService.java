package com.example.demo.service;

import com.example.demo.entity.Product;
import com.example.demo.entity.Sale;
import com.example.demo.repository.ProductRepository;
import com.example.demo.repository.SaleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductService {

    @Autowired
    SaleRepository saleRepository;

    @Autowired
    ProductRepository productRepository;

    public Product store(Product product){
        return productRepository.save(product);
    }

    public Sale getSaltById(int id){
        return saleRepository.findById(id).orElse(null);
    }
}
